<?php
session_start();

/*
Mench

By Samic.
(samic.org)

Created on April 09, 2020
Updated on April 13, 2020

*/

include "database.php";
include "constants.php";

$this_time = time();
$main_page ="
<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01//EN'>
<html manifest='site.appcache'>
<head>
<title>Mench</title>
</head>
<frameset rows='100%' border='0'>
    <frame src='player.php?up={$this_time}'>
</frameset>
</html>";

if (isset($_SESSION["user"]) && isset($_SESSION["game_id"]))  exit($main_page);     // automatically re-login a user that left the page

if (isset($_REQUEST['gameid']))  $_REQUEST['game_id'] = $_REQUEST['gameid'];
if (isset($_REQUEST['id']))  $_REQUEST['game_id'] = $_REQUEST['id'];
if (isset($_REQUEST['game']))  $_REQUEST['game_id'] = $_REQUEST['game'];


if (isset($_REQUEST['game_id'])){

    $_REQUEST['game_id'] = mysqli_real_escape_string($DBlink, $_REQUEST['game_id']);

    $sql = "SELECT `game_id` FROM `game` WHERE game_id='{$_REQUEST['game_id']}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    if (mysqli_num_rows($result) < 1)  ShowText_Exit("Invalid Game ID<br>Click on the invitation link again.");

    $_SESSION["game_id"] = $_REQUEST['game_id'];

}else{
    ShowText_Exit("You need to use an invitation link.<br><br>Click on the invitation link again<br><br>Or<form action='admin.php'><input type='submit' value='start a new group'></form>");
}


$selected_colors = "";
$sql = "SELECT `color` FROM `players` WHERE game_id='{$_REQUEST['game_id']}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
while ($rows = mysqli_fetch_array($result))
    $selected_colors .= $rows['color'];


if (isset($_REQUEST['user'])){

    if (!isset($_REQUEST['color']))  ShowText_Exit("You need to choose a color!<br><br>");

    $_REQUEST['color'] = strtolower(mysqli_real_escape_string($DBlink, $_REQUEST['color']));

    if ((strpos($selected_colors, $_REQUEST['color']) !== false) ||     // if it was in the already selected colors
        (strpos('kbygrwop', $_REQUEST['color']) === false) ||           // if it wasn't one of the acceptable colors
        (strlen($_REQUEST['color']) != 1))
            ShowText_Exit("You need to choose another color!<br><br>");

    $_REQUEST['user'] = mysqli_real_escape_string($DBlink, ucfirst($_REQUEST['user']));

    if ((ctype_alnum($_REQUEST['user']) == false) || (strlen($_REQUEST['user']) < 1) || (strlen($_REQUEST['user']) > 49)){
        ShowText_Exit("That's not an acceptable name. Choose something else.");
    }

    $sql = "SELECT `player` FROM `players` WHERE game_id='{$_REQUEST['game_id']}' AND player='{$_REQUEST['user']}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    if (mysqli_num_rows($result) < 1) {

        $sql = "INSERT INTO `players` (game_id, player, color, p1, p2, p3, p4) VALUES ('{$_REQUEST['game_id']}', '{$_REQUEST['user']}', '{$_REQUEST['color']}', '{$_REQUEST['color']}w1', '{$_REQUEST['color']}w2', '{$_REQUEST['color']}w3', '{$_REQUEST['color']}w4')";
        $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    }//else{
    //    ShowText_Exit("A player is playig with this name right now!<br><br>Choose another name.<br><br>");
    //}

    // Get the latest log
    $sql = "SELECT `log` FROM `game` WHERE game_id='{$_REQUEST['game_id']}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    $rows = mysqli_fetch_array($result);
    $log = $rows['log'];

    $news = date("h:i:s") . " " . $_REQUEST['user'] . " joined the game.";
    $log .= "\n" . $news;
    $sql = "UPDATE `game` SET log='{$log}' WHERE game_id='{$_REQUEST['game_id']}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


    $_SESSION["user"] = $_REQUEST['user']; // you're logged in!
    $_SESSION["color"] = $_REQUEST['color'];
    echo $main_page;


}else{


?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<title>Mench</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<link rel='stylesheet' type='text/css' href='phpmyadmin.css'>
<style type='text/css' media='screen'>
* {
	margin:0;
	padding:0;
}
div#banner {
	position:absolute;
	top:0;
	right:0;
	margin:0;
	background-color:#eee;
	width:150px;
	border-radius:8px;
}
div#banner-content {
	width:150px;
	margin:0 auto;
	padding:10px;
	border:0px;
}
div#main-content {
	padding: 70px;
}
</style>
</head>
<body class='loginform'>
<div class='container'>
	<p>
		<br>
	</p>
	<form method='post' name='login_form' class='login'>
		<fieldset>
			<legend>Join the game</legend>
			<div class='item'>
				<label for='input_username'>Type your name:</label>
				<input type='text' id='user' name='user' size='15' class='textfield'>
                <br>
				<label for='input_username'>Color:</label>

                <select id="color" name='color'>
                    <option value="" disabled="disabled" selected="selected">Please select a color</option>

<?php
    foreach ($color_names as $key => $value){
        if (strpos($selected_colors, $key) !== false){
            echo "                    <option value='{$key}' disabled='disabled'>{$value}</option>\n";
        }else{
            echo "                    <option value='{$key}'>{$value}</option>\n";
        }
    }

?>
            </select>
                <input type='hidden' name='game_id' value='<?php echo $_REQUEST['game_id']; ?>'>
			</div>
		</fieldset>
		<fieldset class='tblFooters'>
			<input value=' Join ' type='submit' id='input_go'>
		</fieldset>
	</form>
</div>
<p>
</p>
<a style='text-decoration: none;' target='_blank' href='https://samic.org'><i><code style='color: rgb(153, 153, 153); font-size:11px'>By Samic.</code></i></a>
<br>
<a style='text-decoration: none;' target='_blank' href='https://gitlab.com/samic130/mench'><i><code style='color: rgb(153, 153, 153); font-size:11px'>(See the source code here)</code></i></a>
<script>document.getElementById('user').focus();</script>
</body>
</html>

<?php 
}
?>
