-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 11, 2020 at 04:21 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mench`
--

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `ID` int(11) NOT NULL,
  `game_id` tinytext CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0',
  `dice1` int(1) NOT NULL DEFAULT '3',
  `dice2` int(1) NOT NULL DEFAULT '6',
  `log` mediumtext CHARACTER SET latin1 COLLATE latin1_bin
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `ID` int(11) NOT NULL,
  `game_id` tinytext CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `player` tinytext CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `color` varchar(1) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `p1` tinytext CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `p2` tinytext CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `p3` tinytext CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `p4` tinytext CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `hand_active` int(1) NOT NULL DEFAULT '0',
  `hand_position` tinytext,
  `hand_picked` tinytext CHARACTER SET ascii COLLATE ascii_bin
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
