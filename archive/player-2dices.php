<?php
session_start();

/*
Mench

By Samic.
(samic.org)

Created on April 09, 2020
Updated on April 11, 2020 

*/

include "database.php";
include "coordinates.php";
date_default_timezone_set('America/Chicago');
$this_time = time();

if (! isset($_SESSION["user"]))  ShowText_Exit("You need to log in.<br><br>Click <a href=index.php>here</a>.");

$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];


function place_piece($piece, $place){
    global $x, $y;
    $style ="
.{$piece} {
  position: absolute;
  top: {$y[$place]}px;
  left: {$x[$place]}px;
  z-index: 20;
}";
    return $style;
}

function place_hand($place, $color){
    global $x, $y;
    $colors = array("k" => '#1c1c1c', "b" => '#3848c2', "y" => '#fff450', "g" => '#25d438',
                    "r" => '#d54c48', "w" => '#985006', "o" => '#f58220', "p" => '#be3dae');
    $hand_y = $y[$place] - 12;
    $dot_y = $hand_y + 9;
    $dot_x = $x[$place] + 18;
    $style ="
.hand_{$color} {
  position: absolute;
  height: 50px;
  top: {$hand_y}px;
  left: {$x[$place]}px;
  z-index: 50;
}
.dot_{$color} {
  position: absolute;
  top: {$dot_y}px;
  left: {$dot_x}px;
  height: 12px;
  width: 12px;
  background-color: {$colors[$color]};
  border-radius: 50%;
  display: inline-block;
  z-index: 55;
  border-style: solid;
  border-width: 1px;
}";
    return $style;
}

function box_css($place, $zindex){
    global $x, $y;
    $this_y = $y[$place] + 5;
    $this_x = $x[$place] + 5;
    $style ="
.{$place}_box {
  position: absolute;
  height: 29px;
  width: 28px;
  top: {$this_y}px;
  left: {$this_x}px;
  z-index: {$zindex};
}";
    return $style;
}



$sql = "SELECT * FROM `game` WHERE game_id='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = $rows['log'];
$version = $rows['version'];
$dice1 = $rows['dice1'];
$dice2 = $rows['dice2'];

if (isset($_GET['dice'])){

    $dice1 = rand(1, 6);
    $dice2 = rand(1, 6);
    $dice_updated = 1;
    $version++;

    $news = date("h:i:s") . " " . $user . " rolled the dice.";
    $log .= "\n" . $news;

    $sql = "UPDATE `game` SET version='{$version}', dice1='{$dice1}', dice2='{$dice2}', log='{$log}' WHERE game_id='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    $sql = "UPDATE `players` SET hand_active='0', hand_position='', hand_picked='' WHERE game_id='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

$_SESSION["mode"] = 'ready';
}


if (isset($_GET['pick']) && isset($_GET['piece'])){

    $user_color = substr($_GET['piece'], 0, 1);
    $user_piece = substr($_GET['piece'], 2, 3);

    if (strpos('kbygrwop', $user_color) === false)  ShowText_Exit("Wrong color!<br><br>Error 101");
    if (strpos('1234', $user_piece) === false)  ShowText_Exit("Wrong piece!<br><br>Error 102");

    $user_piece = 'p' . $user_piece;

    $sql = "SELECT * FROM `players` WHERE game_id='{$game_id}' AND color='{$user_color}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    if (mysqli_num_rows($result) == 1) {
        $rows = mysqli_fetch_array($result);
        $piece_position = $rows[$user_piece];

        $sql2 = "UPDATE `players` SET hand_active='1', hand_position='{$piece_position}', hand_picked='{$user_color}{$user_piece}' WHERE game_id='{$game_id}' AND player='{$user}'";
        $result2 = mysqli_query($DBlink, $sql2) or die(mysqli_error($DBlink));

        $version++;
        $news = date("h:i:s") . " " . $user . " picked a piece.";
        $log .= "\n" . $news;

        $sql2 = "UPDATE `game` SET version='{$version}', log='{$log}' WHERE game_id='{$game_id}'";
        $result2 = mysqli_query($DBlink, $sql2) or die(mysqli_error($DBlink));

        $_SESSION["mode"] = 'place';
    }
}



if (isset($_GET['place']) && isset($_GET['position']) && isset($_SESSION["mode"]) && ($_SESSION["mode"] == 'place')){

    $_GET['position'] = mysqli_real_escape_string($DBlink, $_GET['position']);
    if (!in_array($_GET['position'], $positions))  ShowText_Exit("Wrong position!<br><br>Error 103");


    $sql = "SELECT * FROM `players` WHERE game_id='{$game_id}' AND player='{$user}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    $rows = mysqli_fetch_array($result);
    $hand_picked = $rows['hand_picked'];

    $user_color = substr($hand_picked, 0, 1);
    $user_piece = substr($hand_picked, 1, 3);

    $version++;
    $news = date("h:i:s") . " " . $user . " placed a piece.";
    $log .= "\n" . $news;

    $sql = "SELECT * FROM `players` WHERE game_id='{$game_id}';";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    while ($rows = mysqli_fetch_array($result)){
        for($i=1; $i<5; $i++){
            if ($rows['p' . $i] == $_GET['position']){
                $sql2 = "UPDATE `players` SET p{$i}='{$rows['color']}w{$i}' WHERE ID='{$rows['ID']}'";
                $result2 = mysqli_query($DBlink, $sql2) or die(mysqli_error($DBlink));
                $news = date("h:i:s") . " " . $user . " replaced one of " . $rows['color'] . " pieces.";
                $log .= "\n" . $news;
                break;
            }
        }
    }

    $sql = "UPDATE `players` SET hand_position='{$_GET['position']}' WHERE game_id='{$game_id}' AND player='{$user}';";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    $sql = "UPDATE `players` SET `{$user_piece}`='{$_GET['position']}' WHERE game_id='{$game_id}' AND color='{$user_color}';";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    $sql = "UPDATE `game` SET version='{$version}', log='{$log}' WHERE game_id='{$game_id}'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));

    $_SESSION["mode"] = 'ready';
}


?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html manifest="site.appcache">
<head>
<title>Mench</title>
<meta http-equiv="Cache-Control" content="max-age">
<meta http-equiv="Expires" content="Sat, 01 Jan 2050 1:00:00 GMT">
<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
<link rel='stylesheet' type='text/css' href='phpmyadmin.css'>
<head>
<style type='text/css' media='screen'>
.parent {
  position: relative;
  top: 0;
  left: 0;
  margin: 0 auto;
  width: 50em;
}
.board {
  margin: 0 auto;
  position: relative;
  top: 0;
  left: 0;
  z-index: 1;
}
.dice1 {
  position: absolute;
  height: 50px;
  top: 310px;
  left: 260px;
  z-index: 10;
}
.dice2 {
  position: absolute;
  height: 50px;
  top: 270px;
  left: 320px;
  z-index: 10;
}
<?php

foreach ($start as $key => $value)
    echo place_piece($key, $value);

foreach ($positions as $value){
    if (isset($_SESSION["mode"]) && ($_SESSION["mode"] == 'place')){
        echo box_css($value, 80);
    }else{
        echo box_css($value, 10);
    }
}


$sql = "SELECT * FROM `players` WHERE game_id='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
while ($rows = mysqli_fetch_array($result)){
    if ($rows['hand_active'] == 1)
        echo place_hand($rows['hand_position'], $rows['color']);

    echo place_piece($rows['color'] . 'p1', $rows['p1']);
    echo place_piece($rows['color'] . 'p2', $rows['p2']);
    echo place_piece($rows['color'] . 'p3', $rows['p3']);
    echo place_piece($rows['color'] . 'p4', $rows['p4']);
}



?>
</style>
</head>
<body>
<script src="jquery.min.js" type="text/javascript"></script>
<div class="parent">
    <img class="board" id="board" src="images/board250.gif" height="630px">

    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=kp1"><img class="kp1" src="images/K.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=kp2"><img class="kp2" src="images/K.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=kp3"><img class="kp3" src="images/K.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=kp4"><img class="kp4" src="images/K.png"></a>

    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=bp1"><img class="bp1" src="images/B.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=bp2"><img class="bp2" src="images/B.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=bp3"><img class="bp3" src="images/B.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=bp4"><img class="bp4" src="images/B.png"></a>

    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=yp1"><img class="yp1" src="images/Y.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=yp2"><img class="yp2" src="images/Y.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=yp3"><img class="yp3" src="images/Y.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=yp4"><img class="yp4" src="images/Y.png"></a>

    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=gp1"><img class="gp1" src="images/G.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=gp2"><img class="gp2" src="images/G.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=gp3"><img class="gp3" src="images/G.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=gp4"><img class="gp4" src="images/G.png"></a>

    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=rp1"><img class="rp1" src="images/R.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=rp2"><img class="rp2" src="images/R.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=rp3"><img class="rp3" src="images/R.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=rp4"><img class="rp4" src="images/R.png"></a>

    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=wp1"><img class="wp1" src="images/W.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=wp2"><img class="wp2" src="images/W.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=wp3"><img class="wp3" src="images/W.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=wp4"><img class="wp4" src="images/W.png"></a>

    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=op1"><img class="op1" src="images/O.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=op2"><img class="op2" src="images/O.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=op3"><img class="op3" src="images/O.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=op4"><img class="op4" src="images/O.png"></a>

    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=pp1"><img class="pp1" src="images/P.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=pp2"><img class="pp2" src="images/P.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=pp3"><img class="pp3" src="images/P.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&pick=1&piece=pp4"><img class="pp4" src="images/P.png"></a>

    <?php
    foreach ($positions as $value)
        echo "    <a href='player.php?up={$this_time}&place=1&position={$value}'><div class='{$value}_box'></div></a>\n";


    $sql = "SELECT * FROM `players` WHERE game_id='{$game_id}' AND hand_active='1'";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    while ($rows = mysqli_fetch_array($result)){
        echo "    <div id='hand_{$rows['color']}'><img class='hand_{$rows['color']}' src='images/hand.png'><span class='dot_{$rows['color']}'></span></div>\n";
        echo "    <script>setTimeout(function() {document.getElementById('hand_{$rows["color"]}').style.display = 'none';}, 3000);</script>\n";
    }
    ?>


    <a href="player.php?up=<?php echo $this_time ?>&dice=1"><img class="dice1" src="images/<?php echo $dice1; ?>.png"></a>
    <a href="player.php?up=<?php echo $this_time ?>&dice=1"><img class="dice2" src="images/<?php echo $dice2; ?>.png"></a>

    <?php
    if (isset($dice_updated))
        echo '<audio autoplay><source src="dice.mp3" type="audio/mpeg"></audio>';
    ?>

</div>

<p><br></p>

<table width="100%" border="0">
<tr>
<td width="50%" style="vertical-align:top;padding: 10px;float:right;  margin: 0 auto;width: 15em; margin-right: 20px;">

    <h3>Playing Users:</h3>
    <table border="1" style="border: 1px solid black;border-collapse: collapse;">

    <tr>
    <td style="padding: 10px;" width="120"><b><i>Name</i></b></td>
    <td style="padding: 10px;" width="100"><b><i>Color</i></b></td>
    </tr>


    <?php

    $colors = array("k" => 'Black', "b" => 'Blue', "y" => 'Yellow', "g" => 'Green',
                    "r" => 'Red', "w" => 'Brown', "o" => 'Orange', "p" => 'Purple');

    $colors_html = array("k" => '#1c1c1c', "b" => '#3848c2', "y" => '#fff450', "g" => '#25d438',
                         "r" => '#d54c48', "w" => '#985006', "o" => '#f58220', "p" => '#be3dae');

    $sql = "SELECT * FROM `players` WHERE game_id='{$game_id}' ORDER BY ID";
    $result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
    while ($rows = mysqli_fetch_array($result)) {
        if (!isset($admin))  $admin = $rows['player'];    // the goal here is to define the first user as admin and not anyone else
        if (isset($admin) && ($admin == $user) && ($rows['player'] != $user)){
            echo "
                <tr>
                <td style='padding: 10px;'>
                    <span style='float: left;'>{$rows['player']}</span>
                     &nbsp;&nbsp;
                    <span style='float: right;'>
                    <a href='boot.php?game_id={$game_id}&user_boot={$rows['player']}' target='_blank' title='Boot this player out of the game!'>
                        <span style='font-size: 0.5em;'>X</span>
                    </a>
                    </span>
                </td>
                <td style='padding: 10px;'><b><span style='color:{$colors_html[$rows['color']]}'>{$colors[$rows['color']]}</span></b></td>
                </tr>
                ";
        }else{
            echo "
                <tr>
                <td style='padding: 10px;'>{$rows['player']}</td>
                <td style='padding: 10px;'><b><span style='color:{$colors_html[$rows['color']]}'>{$colors[$rows['color']]}</span></b></td>
                </tr>
                ";
        }
    }
    ?>

    </table>
<small><a href="leave.php">Leave the game</a><br></small>
</td>
<td width="50%" style="vertical-align:top;padding: 10px;">

    <h3>Game log:</h3>
    <textarea id="log" rows="12" cols="35" style="font-size: 13px;"><?php echo substr($log, -2000); ?></textarea>

    <br>
    <small><a href="player.php?up=<?php echo $this_time ?>" title="Use this to refresh the page if it stopped updating">Refresh</a><br></small>
</td>
</tr>
</table>


<script>
var textarea = document.getElementById('log');
textarea.scrollTop = textarea.scrollHeight;

function loadData() {
  var xhttp = new XMLHttpRequest();
  var now = new Date();
  var this_time = now.getTime().toString();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        server_response = JSON.parse(this.responseText);
        server_version = server_response[0];
        document.getElementById("log").innerHTML = server_response[1];

        client_version = <?php echo $version; ?>;
        if (client_version != server_version){
            window.location = "player.php?up=" + this_time;
        }

        var textarea = document.getElementById('log');
        textarea.scrollTop = textarea.scrollHeight;
    }
  };
  xhttp.open("GET", "update.php?up=" + this_time, true);
  xhttp.send();
}

setInterval(function(){
   loadData()
}, 1000);
</script>
</body>
</html>
