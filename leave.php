<?php
session_start();

/*
Mench

By Samic.
(samic.org)

Created on April 09, 2020
Updated on April 11, 2020 

*/

include "database.php";


$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];


// Get the latest log
$sql = "SELECT `log` FROM `game` WHERE game_id='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = $rows['log'];


$news = date("h:i:s") . " " . $user . " left the game.";
$log .= "\n" . $news;
$sql = "UPDATE `game` SET log='{$log}' WHERE game_id='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


$sql = "DELETE FROM `players` WHERE player='{$user}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


session_unset();
session_destroy();
session_write_close();
setcookie(session_name(),'',0,'/');


ShowText_Exit("Thanks for playing!<br>See you later ...");


?>
