<?php
session_start();

/*
Mench

By Samic.
(samic.org)

Created on April 09, 2020
Updated on April 11, 2020 

*/

include "database.php";

if (! isset($_SESSION["user"]))  ShowText_Exit("You cannot access this page directly.");

$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];


// Get the latest log
$sql = "SELECT * FROM `game` WHERE game_id='{$game_id}';";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = substr($rows['log'], -2000);
$version = $rows['version'];


header("Content-type: application/json");
echo json_encode(array($version, $log));

?>
