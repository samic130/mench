<?php
session_start();

/*
Mench

By Samic.
(samic.org)

Created on April 09, 2020
Updated on April 11, 2020 

*/

include "database.php";


$user = $_SESSION["user"];
$game_id = $_SESSION["game_id"];

if (isset($_GET['user_boot'])){
    $user_boot = $_GET['user_boot'];
}else{
    ShowText_Exit("No user was given!");
}


$sql = "SELECT * FROM `players` WHERE game_id='{$game_id}' ORDER BY ID";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
if ($rows['player'] != $user)  ShowText_Exit("You're not the admin!");


// Get the latest log
$sql = "SELECT `log` FROM `game` WHERE game_id='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
$rows = mysqli_fetch_array($result);
$log = $rows['log'];


$news = date("h:i:s") . " " . $user . " booted " . $user_boot;
$log .= "\n" . $news;
$sql = "UPDATE `game` SET log='{$log}' WHERE game_id='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


$sql = "DELETE FROM `players` WHERE game_id='{$game_id}' AND player='{$user_boot}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


ShowText_Exit($user_boot . " is booted!<br>Close this window.");


?>
