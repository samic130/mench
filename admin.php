<?php
session_start();

/*
Mench

By Samic.
(samic.org)

Created on April 09, 2020
Updated on April 11, 2020 

*/

include "database.php";

$game_id = time() - 1000000000;
$game_id = base_convert($game_id, 10, 36);


if (isset($_GET['game_id'])){
    $game_id = mysqli_real_escape_string($DBlink, $_GET['game_id']);
}


$sql = "SELECT `game_id` FROM `game` WHERE game_id='{$game_id}'";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));
if (mysqli_num_rows($result) > 0)  ShowText_Exit("Invalid Game ID<br>Select something else.");

$sql = "INSERT INTO `game` (game_id) VALUES ('{$game_id}')";
$result = mysqli_query($DBlink, $sql) or die(mysqli_error($DBlink));


ShowText_Exit("A new invitation link was created.<br><br>Give this link to the each player:<br><br><a href=https://mench.samic.org/?game_id={$game_id}>https://mench.samic.org/?game_id={$game_id}</a>");

?>
